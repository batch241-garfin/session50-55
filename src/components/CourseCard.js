import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

export default function CourseCard({course}) {

  const { name, description, price, _id } = course;

  // Use the state hook for this component to be ale to stroe its state.
  // States are used to kepp track of information related to individual components.
  // Syntax:
  // const [getter,setter] = useState(initialGetterValue);
  // const [ count, setCount ] = useState(0);
  // const [ seats, seatSeats ] = useState(30);
  // const [ isOpen, setIsOpen ] = useState(true);


  // function enroll()
  // {

  //   setCount(count + 1);
  //   seatSeats(seats - 1);
  //     // if (seats > 0)
  //     // {
  //     //   setCount(count + 1);
  //     //   seatSeats(seats - 1);
  //     // }
  //     // else
  //     // {
  //     //   alert("no more seats!")
  //     // }

  //   // function enroll()
  //   // setcount(count + 1);
  //   // setSeats (seats - 1);
  //   // if(seats == 1){alert("no more seats");
  //   // document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true)}
  // }

  // useEffect() - will allow us to execute a function if the value of seats state changes
  // useEffect(() => {

  //   if (seats == 0)
  //   {
  //     setIsOpen(false);
  //     document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
  //   }

  //   // will run anytime one of the values in the array of the dependencies changes - [seats]
  // }, [seats])
  

  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
        <Card.Text>
          {description}
        </Card.Text>
        <Card.Subtitle>
          Price:
        </Card.Subtitle>
        <Card.Text>
          PhP {price}
        </Card.Text>
        {/*<Card.Text>
          Enrollees: {count}
        </Card.Text>
        <Card.Text>
          Seats: {seats}
        </Card.Text>*/}
        {/*<Button variant="primary">Enroll</Button>*/}
        {/*<Button className="bg-primary" onClick={enroll} id={`btn-enroll-${id}`}>Enroll</Button>*/}
        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
      </Card.Body>
    </Card>
  );
}