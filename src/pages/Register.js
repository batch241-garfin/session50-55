import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';
//import registeredUser from '../data/registeredUser';

export default function Register() {

    // const {user, setUser} = useContext(UserContext);
    //const [ user, setUser ] = useState({email: localStorage.getItem('email')});

    const {user} = useContext(UserContext);
    const re = {};

    // State hooks store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [mobNum, setMobNum] = useState('');

    // State to determine whether submit button will be enabled or not

    const [isActive, setIsActive] = useState(false);

    // Function to simulate user registration
    function registerUser(e)
    {
        // Prevents page from reloading
        e.preventDefault()
        

        //registeredUser.push({email: email, password: password1})

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {

                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: email
                })
            })
            .then(res => res.json())
            .then(data => {


                if (data) {

                    Swal.fire(
                    {
                        title: "Duplicate email found",
                        icon: "error",
                        text: "Please provide a different email."
                    })
                }
                else
                {

                    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {

                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            mobileNo: mobNum,
                            password: password1
                        })
                    })
                    .then(res => res.json())
                    .then(data => {

                        Swal.fire(
                        {
                            title: "Registration successful",
                            icon: "success",
                            text: "Welcome to Zuitt!"
                        })

                        // Clears the form
                        setFirstName('');
                        setLastName('');
                        setEmail('');
                        setMobNum('');
                        setPassword1('');
                        setPassword2('');

                    });
                }
        });
    }
    

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && firstName !== '' && lastName !== '' && mobNum !== '' && mobNum.length == 11 && password1 !== '' && password2 !== '' && password1 === password2)
        {
            setIsActive(true);
            // document.querySelector('#submitBtn').removeAttribute('disabled', true);
        }
        else
        {
            setIsActive(false);
            // document.querySelector('#submitBtn').setAttribute('disabled', true);
        }
    }, [email, firstName, lastName, mobNum, password1, password2]);

//console.log(user.email)
    return (


        (user.id !== null) ?
        <Navigate to="/courses"/>
        :
        <Form onSubmit={(e) => registerUser(e)} >
            <Form.Group controlId="userFirstName">
                <Form.Label>First Name:</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter Firstname"
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userLastName">
                <Form.Label>Last Name:</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter Lastname"
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="userMobNum">
                <Form.Label>Mobile Number:</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter Mobile Number"
                    value={mobNum}
                    onChange={e => setMobNum(e.target.value)}
                    minLength="11"
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password"
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password"
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
	                required
                />
            </Form.Group>

            { isActive ?
            <Button variant="primary" type="submit" id="submitBtn">
            	Submit
            </Button>
            :
            <Button variant="danger" type="submit" id="submitBtn">
                Submit
            </Button>
            }
        </Form>
    )

}