import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

//import registeredUser from '../data/registeredUser';


export default function Login() {

    // Allows us to use the UserContext object and its properties to be used for user validation
    const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    //const navigate = useNavigate();

    function loginUser(e)
    {
    	e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {

            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            //console.log(data.access);

            // If no user information is found, the "access" property will not be available and return undefined
            if (typeof data.access !== "undefined")
            {
                // The JWT will be used to retrieve user information accross the whole frontend application and storing it in the localStorage.
                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access);

                Swal.fire(
                {
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
                }
                else
                {
                Swal.fire(
                {
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })
            }
        });

    	 	// registeredUser.forEach((user) => {

            // 	if (user.email === email && user.password === password)
            // 	{
        			// Set the email of the authenticated user in local storage.
        			// Syntax: localStorage.setItem('propertyName', value)
        			//localStorage.setItem('email', email);

                    // Sets the global user state to have properties obtained from local
                    //setUser({email: localStorage.getItem('email')});

        			setEmail('');
        			setPassword('');
        			//navigate('/')

        			//alert("Successfully Login!");
            // 	}
            // 	else
            // 	{
            // 		setEmail('');
        	// 		setPassword('');
            // 		alert("User not Found!")
            // 	}
            // })
    }

    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {

            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Global user state for validation accross the whole app
            // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation accross whole application.
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    };

    useEffect(() => {

        if(email !== '' && password !== '')
        {
            setIsActive(true);
            // document.querySelector('#submitBtn').removeAttribute('disabled', true);
        }
        else
        {
            setIsActive(false);
            // document.querySelector('#submitBtn').setAttribute('disabled', true);
        }
    }, [email, password]);


    return (

        (user.id !== null) ?
        <Navigate to="/courses"/>
        :
        <Form onSubmit={(e) => loginUser(e)} >
            <Form.Group controlId="userEmail">
                <Form.Label>Email Address:</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password:</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
	                required
                />
            </Form.Group>

            { isActive ?
            <Button variant="success" type="submit" id="submitBtn">
            	Submit
            </Button>
            :
            <Button variant="success" type="submit" id="submitBtn">
                Submit
            </Button>
            }
        </Form>
    )
}