
// import { Row, Col } from 'react-bootstrap';

// import Banner from '../components/Banner';

// export default function NotFound() {

// return (

//     <Row>
//     	<Col className="p-5">
//             <h4>Zuitt Booking</h4>
//             <h1>Page Not Found</h1>
//             <p>Go back to the <a href="/">Homepage</a>.</p>
//         </Col>
//     </Row>
// 	)
// }

import Banner from '../components/Banner';

export default function Error() {

	const data = {

		title: "Error 404 - Page Not Found",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to Home"
	}

	return (

		<Banner data={data}/>
	)
}